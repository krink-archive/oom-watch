
{
  "id": "3e6ad8f36f6505f70f5677cb14ae9b38",
  "name": "dbc13-ca1d-01",
  "oom-watch": {
    "version":"1.0.0.3",
    "datetime":"2018-04-29 23:59:59"
  },
  "notice":
  [
    {
      "type": "Critical",
      "data": "less than 512 MB available memory"
    },
    {
      "type": "Critical",
      "data": "System Swapping 50 percent"
    },
    {
      "type": "Warning",
      "data": "mysqld running pid 1323 using swap 45748"
    },
    {
      "type": "Notice",
      "data": "30 swappiness"
    },
    {
      "type": "Info",
      "data": "2237 MB available memory"
    },
    {
      "type": "Info",
      "data": "7818 MB total memory"
    }
  ]
}

