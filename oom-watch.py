#!/usr/bin/env python

__version__ = 'v1.0.0.3'

import sys
sys.dont_write_bytecode = True

import os
import subprocess

__usage__ = """\
%s process_name

name of the running process to watch/guard
from the kernel oom-killer

This program was written specifically for redhat/centos 7 and mysqld,
where mysqld typically falls victim to the kernel oom-killer.

The mysqld daemon can consume a large amount of system memory,
which gains the process a high oom_score.  The kernel oom-killer
kills processes with high a oom_score.  This script simply
reduces the process oom_score, thereby offering up other processes
necessary for the oom-killer sacrifice.  Thus, allowing the mysqld
process to thwart the oom-killer.

The following output types...
-----------------------------
Info:
Notice:
Warning:
Critical:

""" % sys.argv[0]

def jlog(msg=''):
    global LOG
    if not JSON:
        print str(msg)
    LOG.append(msg)
    return True

def printk(msg=''):
    if not JSON:
        print str(msg)

def get_pid(name):
    pid = None
    try:
        pidof = subprocess.check_output(["pidof", name])
    except subprocess.CalledProcessError as e:
        printk('process not found: ' + str(name))
        sys.exit(1)

    pidList = pidof.split()

    if len(pidList) > 1:
        printk('more than one pid...')
        #print str(pidList)
        printk(str(pidList))
        pidListsorted = sorted(pidList, reverse=True)
        pid = pidListsorted[0]
    else:
        pid = pidList[0]

    return int(pid)

def get_name(pid):
    name = None
    proc_pid_comm_file = '/proc/' + str(pid) + '/comm'
    with open(proc_pid_comm_file, 'r') as comm:
        #name = comm.readlines() #['mysqld\n'] #list
        name = comm.read() #mysqld #str

    return str(name.strip())

def get_oom_score(pid):
    score = None
    proc_oom_score_file = '/proc/' + str(pid) + '/oom_score'

    with open(proc_oom_score_file, 'r') as score_file:
        score = score_file.read()

    return int(score)

def get_system_swap():
    SwapActive = None
    with open('/proc/meminfo', 'r') as proc:
        meminfo = proc.readlines()

    for line in meminfo:
        if line.startswith('MemTotal'):
            MemTotal = int(line.split(':')[1].split()[0])
        if line.startswith('MemFree'):
            MemFree = int(line.split(':')[1].split()[0])
        if line.startswith('MemAvailable'):
            MemAvailable = int(line.split(':')[1].split()[0])
        if line.startswith('SwapTotal'):
            SwapTotal = int(line.split(':')[1].split()[0])
        if line.startswith('SwapFree'):
            SwapFree = int(line.split(':')[1].split()[0])

    #print 'MemTotal ' + str(MemTotal)
    #print 'MemFree ' + str(MemFree)
    #print 'MemAvailable ' + str(MemAvailable)
    #print 'SwapTotal ' + str(SwapTotal)
    #print 'SwapFree ' + str(SwapFree)

    SwapActive = SwapTotal - SwapFree
    #print SwapActive

    try:
        SwapPercent = SwapActive / float(SwapTotal) * 100
    except ZeroDivisionError:
        SwapPercent = 0

    #print SwapPercent
    if SwapPercent > 1 and SwapPercent < 10:
        jlog('Info: System Swapping a little')
    if SwapPercent > 10 and SwapPercent < 50:
        jlog('Warning: System Swapping 10 percent')
    if SwapPercent > 50:
        jlog('Critical: System Swapping 50 percent')

    return SwapActive

def swap_analysis():
    #print 'running swap_analysis...'
    # grep VmSwap /proc/[0-9]*/status
    #output = os.system('grep VmSwap /proc/[0-9]*/status')

    cli = "grep VmSwap /proc/[0-9]*/status"
    vmswap_output = subprocess.Popen(cli, shell=True, stdout=subprocess.PIPE).stdout.readlines()

    swapDict = {}
    count = 0
    for line in vmswap_output:
        line = line.split()
        #print line[0], line[1], line[2]
        item_line = str(line[0])
        item_size = int(line[1])
        item_pid = int(item_line.split('/')[2])

        if int(item_size) == 0:
            #print 'Zero'
            continue
        else:
            count += 1
            swapDict[item_pid] = item_size
            #print 'swapDict: ' + str(count) + ' '  + str(item_pid) + ' ' + str(item_size)

    #for key in swapDict.iterkeys():
    #    print key, swapDict[key]

    try:
        swap_pid_with_most_mem = max(swapDict, key=swapDict.get)
    except ValueError as e: #max() arg is an empty sequence
        #print 'swapDict ' + str(e)
        printk('swapDict ' + str(e))
        swap_pid_with_most_mem = 0

    if swap_pid_with_most_mem != 0:
        value = swapDict[swap_pid_with_most_mem]
    else:
        value = 'swap_pid_with_most_mem was Zero'

    pid_name = get_name(swap_pid_with_most_mem)

    printk('Process with most swap: ' + str(swap_pid_with_most_mem) + ', ' + str(pid_name) + ' size ' + str(swapDict[swap_pid_with_most_mem]))

def list_hightest_oom_scores():
    # highest oom score...
    #pidList = '/proc/[0-9]*/oom_score'

    oomDict = {}
    cli = "grep -v ^0 /proc/[0-9]*/oom_score"
    oom_score_output = subprocess.Popen(cli, shell=True, stdout=subprocess.PIPE).stdout.readlines()
    for line in oom_score_output:
        line = line.split(':')
        #print line[0], line[1]
        item_line = str(line[0])
        item_size = int(line[1])
        item_pid = int(item_line.split('/')[2])
        #print 'item_pid: ' + str(item_pid) + ' item_size: ' + str(item_size)
        #oomDict[item_pid] = item_size
        oomDict[item_size] = item_pid

    try:
        pid_with_most_oom_score = max(oomDict, key=oomDict.get)
    except ValueError as e: #max() arg is an empty sequence
        printk('oomDict ' + str(e))

    printk('oom-killer top 3 oom_scores...')
    x = 3
    for key in sorted(oomDict.iterkeys(), reverse=True):
        _val = str(key)
        _pid = str(oomDict[key])
        _name = get_name(_pid)
        printk(str(_name) + ' has oom_score: ' + str(_val))
        x -= 1
        if x == 0:
            break

    #print type(sorted_r)
    #print str(sorted_r)

def get_high_mem_pids():

#VmPeak:	Peak virtual memory usage
#VmSize:	Current virtual memory usage
#VmLck:	Current mlocked memory
#VmHWM:	Peak resident set size
#VmRSS:	Resident set size
#VmData:	Size of "data" segment
#VmStk:	Size of stack
#VmExe:	Size of "text" segment
#VmLib:	Shared library usage
#VmPTE:	Pagetable entries size
#VmSwap:	Swap space used

#* VmPeak: Peak virtual memory size.
#* VmHWM: Peak resident set size ("high water mark").
#Note that the VmHWM parameter is interesting inasmuch as it signifies the amount of physical memory required for the process at peak times.

        #VmPeak=`grep VmPeak /proc/$pid/status|awk '{print $2}'`
        #VmSize=`grep VmSize /proc/$pid/status|awk '{print $2}'`
        #VmRSS=`grep VmRSS /proc/$pid/status|awk '{print $2}'`

    cli = "grep VmSize /proc/[0-9]*/status"
    vmsize_output = subprocess.Popen(cli, shell=True, stdout=subprocess.PIPE).stdout.readlines()

    vmsizeDict = {}
    count = 0
    for line in vmsize_output:
        line = line.split()
        #print line[0], line[1], line[2]
        item_line = str(line[0])
        item_size = int(line[1])
        item_pid = int(item_line.split('/')[2])
        #vmsizeDict[item_pid] = item_size
        vmsizeDict[item_size] = item_pid

    return vmsizeDict

def check_system_mem_avail():

    with open('/proc/meminfo', 'r') as proc:
        meminfo = proc.readlines()

    for line in meminfo:
        if line.startswith('MemTotal'):
            MemTotal = int(line.split(':')[1].split()[0])
        if line.startswith('MemFree'):
            MemFree = int(line.split(':')[1].split()[0])
        if line.startswith('MemAvailable'):
            MemAvailable = int(line.split(':')[1].split()[0])
        if line.startswith('SwapTotal'):
            SwapTotal = int(line.split(':')[1].split()[0])
        if line.startswith('SwapFree'):
            SwapFree = int(line.split(':')[1].split()[0])

    if MemAvailable < 1:
        jlog('Critical: less than 1 MB available memory ' + str(MemAvailable))

    if MemAvailable < 16384:
        jlog('Critical: less than 16 MB available memory ' + str(MemAvailable))

    if MemAvailable < 131072:
        jlog('Critical: less than 128 MB available memory ' + str(MemAvailable))

    if MemAvailable > 131072 and MemAvailable < 524288:
        jlog('Critical: less than 512 MB available memory ' + str(MemAvailable))

    if MemAvailable > 524288 and MemAvailable < 1048576:
        jlog('Warning: less than 1024 MB available memory ' + str(MemAvailable))

    #echo 1 >/proc/sys/vm/swappiness
    with open('/proc/sys/vm/swappiness', 'r') as proc:
        swappiness = proc.read()
    if swappiness == 1:
        jlog('Info: ' + str(swappiness.strip()) + ' swappiness')
    else:
        jlog('Notice: ' + str(swappiness.strip()) + ' swappiness')

    MemAvailable_MB = MemAvailable / 1024
    jlog('Info: ' + str(MemAvailable_MB) + ' MB available memory ')

    MemTotal_MB = MemTotal / 1024
    jlog('Info: ' + str(MemTotal_MB) + ' MB total memory ')

    return True

def get_smaps_swap(pid):
    swap = None

    # double check via /proc/<pid>/smaps has Swap: (requires root access)
    proc_smaps_file = '/proc/' + str(pid) + '/smaps'
    try:
        with open(proc_smaps_file, 'r') as smaps_file:
            smaps = smaps_file.readlines()
    except IOError as e:
        printk(e)
        printk('Permission Denied, unable to record smaps Swap')
        return swap

    count = 0 
    for line in smaps:
        if line.startswith('Swap'):
            swap = int(line.split(':')[1].split()[0]) 
            #printk(swap)
            count += swap
    
    return swap

def get_myid(idfile=None):
    myid = None
    from xml.etree import ElementTree

    if not os.path.isfile(idfile):
        print 'cannot access ' + str(idfile) + ': No such file or directory'
        #sys.exit(1)
        return myid

    document = ElementTree.parse(idfile)

    pin = document.find('Pin')
    for node in pin.getiterator():
        #print node.tag, node.attrib, node.text, node.tail
        #if debug: print 'node.tag ' + str(node.tag)
        #if debug: print 'node.attrib ' + str(node.attrib)
        #if debug: print 'node.text ' + str(node.text)
        #if debug: print 'node.tail ' + str(node.tail)
        myid = str(node.text)

    #if debug: print 'DEBUG myid ' + myid
    return myid


if __name__ == "__main__":
    LOG = []
    JSON = False

    if sys.argv[1:]:

        if sys.argv[1] == "--version":
            print __version__
            sys.exit(1)
        if sys.argv[1] == "--help":
            print __usage__
            sys.exit(1)
        else:
            name = sys.argv[1]

        if len(sys.argv) > 2:
            if sys.argv[2] == "--json":
                JSON = True
            else:
                print "Unknown option: " + str(sys.argv[2])
                sys.exit(1)
    else:
        print __usage__
        sys.exit(1)


    pid = get_pid(name)
    #print 'pid is ' + str(pid)
    printk(name + ' pid is ' + str(pid))

    oom_score = get_oom_score(pid)
    #print 'oom_score is ' + str(oom_score)
    printk(name + ' oom_score is ' + str(oom_score))
    #print type(oom_score)

    if oom_score > 0:
        list_hightest_oom_scores()
        decrement = str('-' + str(oom_score))
        printk('reduce ' + str(name) + ' oom_score by ' + decrement)
        cli = "echo " + str(decrement) + " >/proc/" + str(pid) + "/oom_score_adj"
        os.system(cli) # this requires root privs
        # new oom_score is...
        new_oom_score = get_oom_score(pid)
        printk(str(name) + ' oom_score currently set to ' + str(new_oom_score))
        jlog('Notice: ' + str(name) + ' oom_score reduced by ' + str(decrement))

        #echo -17 > /proc/2592/oom_adj
        #We can set valid ranges for oom_adj from -16 to +15, and a setting of -17 exempts a process entirely from the OOM killer.
        #http://www.oracle.com/technetwork/articles/servers-storage-dev/oom-killer-1911807.html
        #Apr 26 19:25:08 coredb-ca4-01 kernel: [2491534.057734] sh (47751): /proc/11752/oom_adj is deprecated, please use /proc/11752/oom_score_adj instead.
        #cli = "echo -17 >/proc/" + str(pid) + "/oom_adj"
        #os.system(cli) # this requires root privs
        #printk(str(name) + ' oom_adj set to -17, exempts process from oom-killer')
        #printk('Notice: ' + str(name) + ' oom_adj set to -17')

    #check if pid is using swap...
    proc_status_file = '/proc/' + str(pid) + '/status'
    with open(proc_status_file, 'r') as status_file:
        status = status_file.readlines()

    VmSwap = 0
    for line in status:
        if line.startswith('VmSwap'):
            VmSwap = int(line.split(':')[1].split()[0])

    #print 'VmSwap is ' + str(VmSwap)
    printk(name + ' VmSwap is ' + str(VmSwap))
    if VmSwap > 0:
        #ALERT = 'Warning: ' + str(name) + ' running pid ' + str(pid) + ' using swap '  + str(VmSwap)
        #printk(ALERT)
        printk('VmSwap: ' + str(name) + ' running pid ' + str(pid) + ' using swap '  + str(VmSwap))

        pid_swap = get_smaps_swap(pid)
        printk('Process swap: ' + str(pid_swap))
        if pid_swap > 0:
            jlog('Warning: ' + str(name) + ' running pid ' + str(pid) + ' using swap '  + str(pid_swap))

    #printk('#-----------------------------')

    #check if system is using any swap at all...
    swap = get_system_swap()
    printk('System swap is ' + str(swap))
    if swap > 0:
        #print 'running swap_analysis...'
        swap_analysis()

    #print 'oom-killer top 3 high_mem...'
    printk('oom-killer top 3 high_mem...')
    x = 3
    highMemDict = get_high_mem_pids()
    for key in sorted(highMemDict.iterkeys(), reverse=True):
        #print str(key) + ' '  + str(highMemDict[key])
        _val = str(key)
        _pid = str(highMemDict[key])
        _name = get_name(_pid)
        printk(str(_name) + ' has high_mem: ' + str(_val))
        x -= 1
        if x == 0:
            break

    #print 'oom-killer top 3 oom_score...'
    list_hightest_oom_scores()

    #check system availble memory...
    #MemAvailable
    check_system_mem_avail()

    if JSON:
        myid = get_myid('/etc/ueserveradmin.conf')
        #print str(myid)

        #import socket
        #myname = str(socket.gethostname())
        myname = str(os.uname()[1])

        #now = 'YYYY-MM-DD HH:MM:SS'
        #import datetime
        #now = datetime.datetime.now()
        import time
        now = str(time.strftime("%Y-%m-%d")) + ' ' + str(time.strftime("%H:%M:%S"))

        print '{'
        print '  "id":"' + str(myid) + '",'
        print '  "name":"' + str(myname) + '",'
        print '  "oom-watch": {'
        print '    "version":"' + str(__version__) + '",'
        print '    "datetime":"' + str(now) + '"'
        print '  },'
        print '  "notice":'
        print '  ['
        count = len(LOG)
        for line in LOG:
            ALERT = line.split(':')[0]
            INFO  = line.split(':')[1:]

            print '    {'
            print '      "type":"' + str(ALERT) + '",'
            print '      "data":"' + str(''.join(INFO).strip()) + '"'
            
            count -= 1
            if count:
                print '    },'
            else:
                print '    }'
        print '  ]'
        print '}'



    #print 'GOOD.BYE'
    sys.exit(0)

# https://www.freedesktop.org/software/systemd/man/systemd.exec.html
#OOMScoreAdjust=
#    Sets the adjustment level for the Out-Of-Memory killer for executed processes. Takes an integer between -1000 (to disable OOM killing for this process) and 1000 (to make killing of this process under memory pressure very likely). See proc.txt for details.

